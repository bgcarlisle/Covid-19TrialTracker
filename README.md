# Covid-19TrialTracker

A web app that tracks clinical trials that have been terminated, suspended or withdrawn due to Covid-19

## System requirements

This web app was designed for a LAMP stack using PHP v. 7.3. It may work on other setups, but this is what it was designed and tested on.

## Setup

First, create the tables as defined in `db-schema.sql`.

You will also need to create a file called `config.php` in the root web directory with the following contents:

```
<?php

session_start();

define('DB_USER', 'user-name');
define('DB_PASS', 'password');
define('DB_NAME', 'db-name');
define('DB_HOST', 'db-host');
define('ABS_PATH', '/absolute-path-with-trailing-slash/');
define('SITE_URL', 'https://your-url-dot-com/');

date_default_timezone_set ('America/Montreal');

include_once (ABS_PATH . "functions.php");

?>
```

In order to prevent non-authorized people from accessing the admin tool, edit the `.htaccess` file in the `admin/` directory to point to an `.htpasswd` file that is not accessible to the web.

Run the `first-run.php` file, changing the bounds of the search query to scrape the trials for a month at a time or so, in order to prevent your server from timing out part-way. Delete `first-run.php` once this step is done.

Rename `cron.php` to something that no one will guess, e.g. `cron-810d437f77829b6c2ae2b121824242b0e9bf423439e995234a05de7c73901ae0.php`. I recommend using a random number generator. This will keep a malicious user from repeatedly pinging it. Set a cron job to call this PHP script every day.

You will also need to edit `cron.php` to save the SQL dump files to a new directory that is not accessible to the web.

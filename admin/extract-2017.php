<?php

// Check on its status while running with:
// SELECT `id`, `nct_id`, `include_2017`, `datestopped_2017`, `status_2017`, `restarted_2017`, `restartdate_2017` FROM `trials_comparator` WHERE `restarted_2017` = 1 ORDER BY `restarted_2017` DESC

include_once ("../config.php");

$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$trials = get_unextracted_2017_cohort_trials ( 25 );

foreach ($trials as $trial) {

    echo $trial['nct_id'] . "<br>\n";

    $nct_id = $trial['nct_id'];

    $ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;

    $remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

    if ( $remote_index_file ) {

	$restarted = FALSE;

	echo "Loaded index<br>\n";

	// This extracts a date for each of the versions of the registry entry
	$regex = '/>([A-Za-z0-9, ]+)<\/a>/';
	
	preg_match_all (
	    $regex,
	    $remote_index_file,
	    $dates,
	    PREG_UNMATCHED_AS_NULL
	);

	$sections = preg_split(
	    $regex,
	    $remote_index_file,
	    -1, // no limit
	    PREG_SPLIT_NO_EMPTY
	);

	// So that the record is only updated once per trial
	// I.e. catching only the *first* time the trial stops
	$db_updated = FALSE;
	
	foreach ($dates[1] as $version_index=>$version_date) {

	    echo date("Y-m-d", strtotime($version_date)) . "<br>\n";

	    if ( ! $db_updated ) {
		
		$version_index = $version_index++;

		if ( strtotime($version_date) > strtotime($trial['datestopped_2017']) ) {
		    // If this version is after the stop date

		    echo "After trial stop date<br>";

		    if (
			(strpos($sections[$version_index+1], "Suspended -->") | strpos($sections[$version_index+1], "Terminated -->") | strpos($sections[$version_index+1], "Withdrawn -->"))
			& ! (strpos($sections[$version_index+1], "Terminated --> Suspended") | strpos($sections[$version_index+1], "Withdrawn --> Suspended") | strpos($sections[$version_index+1], "Suspended --> Terminated") | strpos($sections[$version_index+1], "Withdrawn --> Terminated") | strpos($sections[$version_index+1], "Terminated --> Withdrawn") | strpos($sections[$version_index+1], "Suspended --> Withdrawn"))
		    ) {

			echo "Restarted<br>";

			// If the status changes from Suspended, Terminated or Withdrawn
			// And it isn't changing to Suspended, Terminated or Withdrawn

			echo "Restarted on " . $version_date . "<br>\n";

			if (extract2017cohort ($trial['id'], 1, date("Y-m-d", strtotime($version_date))) ) {
			    $db_updated = TRUE;
			}

			$restarted = TRUE;

		    }

		}
		
	    }
	    
	}

	if ( ! $restarted ) {
	    extract2017cohort ( $trial['id'], 0, NULL );
	}

	echo "<br>\n";
	
    } else {
	echo "<p>Could not load the index file for " . $nct_id . "</p>";
    }
    
}

if ( count ($trials) > 0 ) {
?><script>
   setTimeout(function () {
       location.reload();
   }, 0); // Set this to a number of thousandths of a second to wait between refreshes
</script><?php
	 }

	 ?>

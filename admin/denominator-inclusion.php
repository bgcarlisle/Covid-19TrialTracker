<?php

include_once ("../config.php");

$trials = get_unscreened_denominator_trials ( 5 );
$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

foreach ($trials as $trial) {

    echo $trial['nct_id'] . "<br>\n";

    $nct_id = $trial['nct_id'];

    $ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;

    $remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

    if ($remote_index_file) {

        // Extract a date for each of the versions of the registry entry

        $regex = '/>([A-Za-z0-9, ]+)<\/a>/';

        preg_match_all (
            $regex,
            $remote_index_file,
            $dates,
            PREG_UNMATCHED_AS_NULL
        );

        echo "<table>";

        foreach ($dates[1] as $version_index=>$version_date) {

            echo "<tr>";

            echo "<td>" . date("Y-m-d", strtotime($version_date)) . "</td>";

	    $version = $version_index + 1;
	    
	    echo '<td><input class="' . $nct_id . '" type="hidden" nct_id=' . $nct_id . ' date=' . date("Y-m-d", strtotime($version_date)) . ' version="' . $version . '" id="' . $nct_id . '-' . $version . '" value="">' . '</td>';

            echo "</tr>\n";

        }

        echo "</table>\n\n";

    } else {
        echo "Error loading index<br>\n";
    }


}

?><!-- jQuery -->
<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>
<script>
 
 function process_trial_record ( nct_id, version_index ) {

     $.ajax ({
	 url: '<?php echo SITE_URL; ?>' + 'admin/denominator-inclusion-date.php',
	 type: 'post',
	 data: {
	     nctid: nct_id,
	     versionindex: version_index
	 },
	 dataType: 'html'
     }).done ( function (response) {

	 $('#' + nct_id + '-' + version_index).val(response);

	 number_of_
	 
	 $('.' + nct_id).each ( function () {
	     
	 });
	 
	 if () {
	 }
	 
	 setTimeout(function () {

	     
	 }, 5000);
	 
     });
     
 }
 
 $(document).ready( function () {

     // console.log($('input:first').attr('id'));

     // the first trial to work with

     nct_id = $('input:first').attr('nct_id');
     version_index = $('input:first').attr('version');

     process_trial_record ( ncti_id, version_index );
     
 });
</script>

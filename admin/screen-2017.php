<?php

// Check status while running with this:
// SELECT `id`, `nct_id`, `include_2017`, `datestopped_2017`, `status_2017` FROM `trials_comparator` WHERE `include_2017` = 1

include_once ("../config.php");

$date_of_interest = "2017-12-01";
$cutoff_date = "2018-11-30";
$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$trials = get_unscreened_2017_cohort_trials ( 25 );

foreach ($trials as $trial) {

    echo $trial['nct_id'] . "<br>\n";

    $nct_id = $trial['nct_id'];

    $ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;

    $remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

    if ( $remote_index_file ) {

	$stopped = FALSE;

	echo "Loaded index<br>\n";

	// This extracts a date for each of the versions of the registry entry
	$regex = '/>([A-Za-z0-9, ]+)<\/a>/';
	
	preg_match_all (
	    $regex,
	    $remote_index_file,
	    $dates,
	    PREG_UNMATCHED_AS_NULL
	);

	$sections = preg_split(
	    $regex,
	    $remote_index_file,
	    -1, // no limit
	    PREG_SPLIT_NO_EMPTY
	);

	// So that the record is only updated once per trial
	// I.e. catching only the *first* time the trial stops
	$db_updated = FALSE;
	
	foreach ($dates[1] as $version_index=>$version_date) {

	    echo date("Y-m-d", strtotime($version_date)) . "<br>\n";

	    if ( ! $db_updated ) {
		
		$version_index = $version_index++;

		if ( strtotime($version_date) >= strtotime($date_of_interest) ) {
		    // If this version is after the date of interest

		    echo "After date of interest<br>";

		    if ( strtotime($version_date) <= strtotime($cutoff_date) ) {
			// If this version is before the cutoff date

			echo "Before cutoff<br>";
			if (
			    (strpos($sections[$version_index+1], " --> Suspended") | strpos($sections[$version_index+1], " --> Terminated") | strpos($sections[$version_index+1], " --> Withdrawn"))
			    & ! (strpos($sections[$version_index+1], "Terminated --> Suspended") | strpos($sections[$version_index+1], "Withdrawn --> Suspended") | strpos($sections[$version_index+1], "Suspended --> Terminated") | strpos($sections[$version_index+1], "Withdrawn --> Terminated") | strpos($sections[$version_index+1], "Terminated --> Withdrawn") | strpos($sections[$version_index+1], "Suspended --> Withdrawn"))
			) {

			    echo "Stopped<br>";

			    // If the status changes to Suspended, Terminated or Withdrawn
			    // And it isn't changing from Suspended, Terminated or Withdrawn

			    $status = NULL;
			    
			    if (strpos($sections[$version_index+1], " --> Suspended")) {
				$status = "S";
			    }
			    if (strpos($sections[$version_index+1], " --> Terminated")) {
				$status = "T";
			    }
			    if (strpos($sections[$version_index+1], " --> Withdrawn")) {
				$status = "W";
			    }

			    echo "Stopped on " . $version_date . "<br>\n";

			    if (screen2017cohort ($trial['id'], 1, date("Y-m-d", strtotime($version_date)), $status) ) {
				$db_updated = TRUE;
			    }

			    $stopped = TRUE;

			}
		    }

		}
		
	    }
	    
	}

	if ( ! $stopped ) {
	    screen2017cohort ( $trial['id'], 0, NULL, NULL );
	}

	echo "<br>\n";
	
    } else {
	echo "<p>Could not load the index file for " . $nct_id . "</p>";
    }
    
}

if ( count ($trials) > 0 ) {
?><script>
   setTimeout(function () {
       location.reload();
   }, 0); // Set this to a number of thousandths of a second to wait between refreshes
</script><?php
	 }

	 ?>

---
title: Closure of clinical trials studying Covid-19 due to poor patient accrual or research coordination
author: Benjamin Gregory Carlisle PhD
csl: nature.csl
bibliography: Closure C19.bib
---

**Abstract**

As of August 31, 2020, there were 1414 interventional clinical trials of Covid-19 that did not accept healthy volunteers registered on ClinicalTrials.gov. The following is an analysis of those registry entries that were changed to terminated, suspended or withdrawn by October 31, 2020. Key information was automatically extracted and reasons for stopping each trial were manually processed. Ninety-five Covid-19 trials (6.7%) enrolling a total of 2058 patients were terminated, suspended or withdrawn. Thirty-four (36%) of the clinical trials that stopped explicitly cited accrual or coordination issues, and the remaining 61 (64%) cited other reasons for stopping. A higher proportion of the trials that stopped due to accrual or coordination were multi-centric studies (65% vs 34%, $p=0.0086$) and industry funded (24% vs 16%, $p=0.56$), however the difference for industry funding was not statistically significant. Closures of clinical trials of Covid-19 due to inadequate patient accrual or coordination may have been preventable, and represent statistical underpowering of a clinical trial, which places the social and scientific value of the data that it produces at risk. While a speedy response from researchers to the Covid-19 pandemic is of the utmost importance, lack of coordination may mean that research questions cannot be answered because a trial is stopped or underpowered. This represents potential research waste and inefficiency, a low degree of scientific value, potentially threats to the validity of patient consent and may upset the ethical justification of the trial itself.

# Introduction

The 2019 outbreak of the novel coronavirus SARS-CoV-2 causing Covid-19 that became a global pandemic in early 2020 brought with it a shift in research priorities, with billions of dollars redirected toward Covid-19 research[@KaiserJun.3NIHgrapplesrush2020] and hundreds of interventional trials launched.[@MullardFloodedtorrentCOVID192020]
The scale and scope of clinical trial activities studying Covid-19 has raised concerns regarding their robustness and design, as well as their feasibility and coordination.[@Londonpandemicresearchexceptionalism2020;@Leequalityreportedsample2020;@Janiaudworldwideclinicaltrial2020]
The closure of clinical trials due to poor patient accrual has always been a challenge for clinical trial conduct,[@carlisle_accrual_2015;@ChengSenseUrgencyEvaluating2010;@KornAccrualExperienceNational2010] and this continues to be the case for the first trials studying Covid-19 that have been terminated, suspended or withdrawn due to patient accrual or research coordination issues.

Because the research landscape of Covid-19 changes quickly and is flush with many treatment candidates, clinical trials pursuing hypotheses that are unpromising should be closed as quickly as possible.
However, closures due to inadequate patient accrual or coordination may have been preventable.
Trials that fail to reach planned recruitment targets represent possible statistical underpowering of a clinical trial.
Inadequate recruitment or poor coordination of clinical trials leading to closure places the social and scientific value of the data that they produce—if any—at risk.
This may undermine the ethical justification of a clinical trial, vitiate the consent of patients who enrolled and may also represent research waste and inefficiency.

The following is an analysis of registry entries for studies investigating Covid-19 on ClinicalTrials.gov that have been changed to terminated, suspended or withdrawn.

# Methods

All registry entries for interventional trials with a Covid-19 indication that do not accept healthy volunteers, first posted between January 1, 2020 and August 31, 2020 were downloaded from ClinicalTrials.gov.
Clinical trials that had an overall status that was changed to withdrawn, suspended or terminated before October 31, 2020 were manually coded for reasons citing accrual failure or coordination issues in the "why stopped" field (e.g. NCT04272710, "Similar projects have been registered, and it needs to be withdrawn" or NCT04285190, "The COVID-19 pandemic in China has ended completely. As a result, patient recruiting is impossible in China.").
Other fields such as phase, patient allocation, intervention model, primary purpose, and lead sponsor agency class were automatically extracted from the clinical trial record.
The earliest anticipated enrolment was taken from the first historical version of the clinical trial record that is available on ClinicalTrials.gov, and the latest actual enrolment is the actual enrolment figure given at the date of termination, suspension or withdrawal, if any.

Closure rationale data were collected on a daily basis using code that was repurposed from another project that is available online (https://codeberg.org/bgcarlisle/Covid-19TrialTracker).[@CarlisleBenjaminGregoryCovid19TrialTracker]
The code specific to this analysis is also available in this project repository, and the data set for this analysis are included in supplemental materials.

Inferential tests were done on an exploratory basis only, defining $p<0.05$ as statistically significant.
There was no correction for statistical multiplicity.

# Results

A total of 1414 interventional clinical studies investigating Covid-19 were registered on ClinicalTrials.gov before August 31, 2020.
Among these, 95 clinical trials (6.7%) were terminated, suspended or withdrawn before October 31, 2020.
Thirty-four (2.4%) of the clinical trials that stopped explicitly cited accrual or coordination issues, and the remaining 61 4.3%) cited other reasons for stopping.
(See Table 1 and Figure 1.)

Among these trials, 2058 patients were enrolled in total: 944 in trials that stopped due to accrual or coordination, and 1114 in trials that stopped for other reasons.
The mean anticipated enrolment for trials that stopped due to accrual or coordination issues was 445 (range 15-2700), and the mean anticipated enrolment for trials that stopped for other reasons was 415 (range 10-3312).

More than one third of the studies that stopped (k=37, 39%) were testing hydrochloroquine or chloroquine as monotherapy or in combination.
(See supplementary data set.)

A higher proportion of the trials that stopped due to accrual or coordination were multi-centric studies (65% vs 34%, $p=0.0086$) and industry funded (24% vs 16%, $p=0.56$), however the difference for industry funding was not statistically significant.
On average, clinical trials that stopped due to accrual or coordination issues took 14.0 weeks to be stopped, whereas clinical trials that stopped for other reasons took an average of 12.8 weeks, however this difference is also not statistically significant ($p=0.52$).

# Discussion

More than 1 in 20 of the trials in this sample were terminated, suspended or withdrawn, representing a significant amount of institutional resources extended toward clinical trials that were ultimately unable to be completed.
The total enrolment figure of 2058 patients also represents a significant amount of patient investment as well as potential opportunity costs, as these patients may have been unable to enrol in other clinical trials.
While some of these trial closures are legitimate responses to evolving knowledge about Covid-19, accrual failures and coordination issues are potentially avoidable.

Failure to accrue sufficient patients may come about for many reasons, such as a regional downturn in Covid-19 cases, a mis-estimation of the number of available patients, or competition from other clinical trials.
When a trial fails to accrue sufficient patients, this places the scientific value of the trial at risk, as the investigators may no longer be statistically powered to answer their research question.
The ethical justification of a clinical trial is also jeopardized in cases of accrual failure, as the risks and burdens that are taken on by patients are generally considered to be balanced by the trial's potential knowledge gains.
While it is true that the value of underpowered studies may be rescued by statistical pooling of their data with other trials, as in a meta-analysis, it is unlikely that data from a clinical trial that was terminated early will be published or reported.

To address this ethical issue, institutional review boards might consider requesting an analysis from investigators regarding whether there will be sufficient numbers of patients to justify launching a trial.
Some institutions may need to prioritize some research efforts over others, and coordinate with other institutions in order to ensure that trials that have no hope of reaching their target recruitment numbers due to lack of coordination are not started in the first place.

More than one third of the studies that stopped were testing hydroxychloroquine or chloroquine, anti-malarials that were early favourites for repurposing in Covid-19 that fell out of favour.
The closure of clinical trials due to the progress of science is appropriate and in some ways, unavoidable.
A trial should be terminated when the question it sets out to answer is no longer relevant, or when there is good reason to think that the risk to patients has increased.

While there is now good reason to terminate clinical studies of hydrochloroquine or chloroquine,[@BorbaEffectHighvs2020;@ChloroquineHydroxychloroquineCoronavirus] one of the trials that was not stopped due to accrual or coordination issues, NCT04329611, cites a now-retracted registry analysis of hydrochloroquine[@MehraRETRACTEDHCQ2020] in its rationale for suspension.
This registry analysis gained notoriety after an external investigation found inconsistencies in the data.[@Covid19Lancetretracts2020]

The fact that falsified data brought about the closure of clinical trials, however, is worrisome, despite the fact that other, more reliable evidence would now justify closing these trials.
Higher standards for observational evidence, such as pre-registration, confirmation of data sourcing by journals prior to publication and tighter scrutiny before incorporating observational evidence into decisions that affect patients, such as trial closures, may be appropriate.

Because this report is based on an analysis of ClinicalTrials.gov registry entries, it is limited by how up-to-date clinical trial investigators are at updating their registry entries.
This study is also limited in that there are other clinical trial registries in which studies of Covid-19 may be registered internationally, however they do not provide data regarding the rationale for why a clinical trial is stopped.
Hence, the numbers reported here represent a lower bound on the number of clinical trials of Covid-19 that have been stopped due to inadequate patient accrual or research coordination issues.
While only 34 clinical trials of Covid-19 enrolling 944 patients stopped and explicitly cited accrual or coordination issues, the termination, suspension or withdrawal of a clinical trial is a very insensitive instrument for assessing populations of clinical trials for accrual issues.
Even trials that go on to completion may do so without accruing their full anticipated enrolment goal, or even sufficient patients to answer their research question.[@carlisle_accrual_2015]

While a speedy response from researchers to the Covid-19 pandemic is of the utmost importance, uncoordinated research may mean that investigators are not be able to answer their research questions. Clinical trials that stall in accrual represent potential research waste and inefficiency, may not produce socially or scientifically valuable knowledge, and may upset the ethical justification of the trial itself.

# Table

Table 1. Sample details for clinical trials of Covid-19 that were terminated, suspended or withdrawn before 2020-08-31.

# Figure

Figure 1. Clinical trials of Covid-19 that were terminated, suspended or withdrawn before 2020-08-31. Points represent start dates, and line segments indicate time from start date until the trial registry entry was marked as terminated, suspended or withdrawn. Solid lines indicate clinical trials that were stopped due to accrual or coordination issues, and dashed lines indicate trials that were stopped for other reasons.

# References

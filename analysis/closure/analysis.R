library(tidyverse)

setwd("~/Projects/Covid-19TrialTracker/Trials stopped by Covid-19 Analysis/closure/")

import_date <- "2020-11-04"

### trials <- read_csv("output.csv")
trials <- read_csv(
    paste0("output-", import_date, ".csv")
)

### Number of c19 trials in sample
nrow(trials)

stopped <- trials %>%
  filter(stopped)

### Number of trials that stopped
nrow(stopped)

stopped_accru_coord <- stopped %>%
  filter(c19_accru_coord_stop == 1)

### Number of trials that stopped for accrual or coordination reasons
nrow(stopped_accru_coord)

stopped_other <- stopped %>%
  filter(c19_accru_coord_stop == 0)

### Number of trials that stopped for other reasons
nrow(stopped_other)

### Number of trials testing HCQ or CQ alone or in combination
n_hcq_or_cq <- stopped %>%
  filter(hcq_or_cq == 1) %>%
  nrow()
n_hcq_or_cq
paste0(format(100 * n_hcq_or_cq / nrow(stopped), digits=2), "%")

### overall status

stopped_accru_coord$overall_status %>%
    as.factor() %>%
    summary()

stopped_other$overall_status %>%
    as.factor() %>%
    summary()

### time to closure

                                        # accru/coord
stopped_accru_coord$time_length <- stopped_accru_coord$date_stopped - stopped_accru_coord$c19_first_reg

stopped_accru_coord$time_length %>%
    mean()/7

stopped_accru_coord$time_length %>%
    min()/7

stopped_accru_coord$time_length %>%
    max()/7

                                        # other
stopped_other$time_length <- stopped_other$date_stopped - stopped_other$c19_first_reg

stopped_other$time_length %>%
    mean()/7

stopped_other$time_length %>%
    min()/7

stopped_other$time_length %>%
    max()/7


### earliest anticipated enrol

                                        # accru/coord

stopped_accru_coord$earliest_anticipated_enrol %>%
    mean(na.rm=TRUE)

stopped_accru_coord$earliest_anticipated_enrol %>%
    min(na.rm=TRUE)

stopped_accru_coord$earliest_anticipated_enrol %>%
    max(na.rm=TRUE)

                                        # other

stopped_other$earliest_anticipated_enrol %>%
    mean(na.rm=TRUE)

stopped_other$earliest_anticipated_enrol %>%
    min(na.rm=TRUE)

stopped_other$earliest_anticipated_enrol %>%
    max(na.rm=TRUE)

### latest actual enrol

                                        # accru/coord

stopped_accru_coord$latest_actual_enrol %>%
    sum(na.rm=TRUE)

stopped_accru_coord$latest_actual_enrol %>%
    mean(na.rm=TRUE)

stopped_accru_coord$latest_actual_enrol %>%
    min(na.rm=TRUE)

stopped_accru_coord$latest_actual_enrol %>%
    max(na.rm=TRUE)

                                        # other

stopped_other$latest_actual_enrol %>%
    sum(na.rm=TRUE)

stopped_other$latest_actual_enrol %>%
    mean(na.rm=TRUE)

stopped_other$latest_actual_enrol %>%
    min(na.rm=TRUE)

stopped_other$latest_actual_enrol %>%
    max(na.rm=TRUE)


### phase

stopped_accru_coord$phase %>%
    as.factor() %>%
    summary() / 34

stopped_other$phase %>%
    as.factor() %>%
    summary() / 61*100

### allocation

stopped_accru_coord$allocation %>%
    as.factor() %>%
    summary() *100/34

stopped_other$allocation %>%
    as.factor() %>%
    summary()

### intervention model

stopped_accru_coord$intervention_model %>%
    as.factor() %>%
    summary() *100/34

stopped_other$intervention_model %>%
    as.factor() %>%
    summary() *100/61

### primary purpose

stopped_accru_coord$primary_purpose %>%
    as.factor() %>%
    summary() / 34*100

stopped_other$primary_purpose %>%
    as.factor() %>%
    summary() /61*100

### sponsor

stopped_accru_coord$sponsor_agency_class %>%
    as.factor() %>%
    summary() *100/34

stopped_other$sponsor_agency_class %>%
    as.factor() %>%
    summary()

### single or multi centre

stopped_accru_coord$is_multi_centre %>%
    summary()

stopped_other$is_multi_centre %>%
    summary()


### number of arms

stopped_accru_coord$number_of_arms %>%
    as.factor() %>%
    summary()

stopped_other$number_of_arms %>%
    as.factor() %>%
    summary()


### supplementary table

supp_table <- trials %>%
  filter(stopped)

supp_table$id <- NULL
supp_table$Title <- NULL
supp_table$Status <- NULL
supp_table$Acronym <- NULL
supp_table$Rank <- NULL
supp_table$when_indexed <- NULL
supp_table$primary_completion_date <-NULL
supp_table$primary_completion_date_type <-NULL
supp_table$brief_title <- NULL
supp_table$official_title <- NULL
supp_table$brief_summary <- NULL
supp_table$study_type <- NULL
supp_table$include <- NULL
supp_table$include_updated <- NULL
supp_table$covid19_explicit <- NULL
supp_table$last_update_submitted <- NULL
supp_table$restartexpected <-NULL
supp_table$detailed_description <- NULL
supp_table$cancer_indication <- NULL
supp_table$neuro_indication <- NULL
supp_table$cv_indication <- NULL
supp_table$pain_indication <- NULL
supp_table$c19_indication <- NULL
supp_table$masking <- NULL
supp_table$enrollment_type <- NULL
supp_table$enrollment <- NULL
supp_table$`Study Results` <- NULL
supp_table$Conditions <- NULL
supp_table$Interventions <- NULL
supp_table$`Outcome Measures` <- NULL
supp_table$`Sponsor/Collaborators` <- NULL
supp_table$Gender <- NULL
supp_table$Age <- NULL
supp_table$Phases <- NULL
supp_table$Enrollment <- NULL
supp_table$`Funded Bys` <- NULL
supp_table$`Study Type` <- NULL
supp_table$`Study Designs` <- NULL
supp_table$`Other IDs` <- NULL
supp_table$`Start Date` <- NULL
supp_table$`Primary Completion Date` <- NULL
supp_table$`Completion Date` <- NULL
supp_table$`First Posted` <- NULL
supp_table$`Results First Posted` <- NULL
supp_table$`Last Update Posted` <- NULL
supp_table$Locations <- NULL
supp_table$`Study Documents` <- NULL
supp_table$URL <- NULL
supp_table$number_of_arms <- NULL
supp_table$number_of_locations <- NULL
supp_table$contains_multi <- NULL
supp_table$contains_single <- NULL
supp_table$title_contains_multi <- NULL
supp_table$title_contains_single <- NULL
supp_table$description_contains_multi <- NULL
supp_table$description_contains_single <- NULL
supp_table$design_contains_multi <- NULL
supp_table$design_contains_single <- NULL
supp_table$to_check_multi <- NULL
supp_table$to_check_single <- NULL
supp_table$stopped <- NULL
supp_table$hcq_or_cq <- NULL

multi_label <- function ( db_value ) {
  if ( db_value == TRUE) {
    return("Yes")
  } else {
    return("No")
  }
}

supp_table$multi_arm <- sapply(
    supp_table$is_multi_arm,
    multi_label
)
supp_table$is_multi_arm <- NULL

supp_table$multi_centre <- sapply(
    supp_table$is_multi_centre,
    multi_label
)
supp_table$is_multi_centre <- NULL

supp_table$rationale <- supp_table$why_stopped %>%
    str_replace_all(
        pattern="\\\\n",
        replacement=" "
    ) %>%
    trimws()

supp_table$why_stopped <- NULL

accru_coord_label <- function ( db_value ) {
  if ( db_value == 1) {
    return("Yes")
  } else {
    return("No")
  }
}

supp_table$accru_coord <- sapply(
  supp_table$c19_accru_coord_stop,
  accru_coord_label
)

supp_table$c19_accru_coord_stop <- NULL

supp_table <- supp_table[order(supp_table$date_stopped),]

                                        # col names
supp_table <- supp_table %>%
    rename(`NCT Number` = nct_id) %>%
    rename(Status = overall_status) %>%
    rename(`Anticipated enrolment` = earliest_anticipated_enrol) %>%
    rename(`Actual enrolment` = latest_actual_enrol) %>%
    rename(Phase = phase) %>%
    rename(Allocation = allocation) %>%
    rename(`Intervention model` = intervention_model) %>%
    rename(`Primary purpose` = primary_purpose) %>%
    rename(`Date stopped` = date_stopped) %>%
    rename(`Intervention` = c19_drugname) %>%
    rename(`First registration date` = c19_first_reg) %>%
    rename(`Lead sponsor agency class` = sponsor_agency_class) %>%
    rename(`Multi arm` = multi_arm) %>%
    rename(`Multi centre` = multi_centre) %>%
    rename(`Why stopped` = rationale) %>%
    rename(`Stopped for accrual or coordination reasons` = accru_coord)

supp_table %>%
    write_csv(
        paste0("supp-table-", import_date, ".csv")
    )


### inferential tests

                                        # lead sponor

prop.test(
    x=c(8,10),
    n=c(34,61)
)

                                        # multi centre

prop.test(
    x=c(22,21),
    n=c(34,61)
)

                                        # time to closure
t.test(
    stopped_accru_coord$time_length/7,
    stopped_other$time_length/7
)

### first reg dates double-check

import_date = "2020-11-04"

# Trials in the Covid arm of the project
double_check <- read_delim(
  paste0("../data/covid-arm-", import_date , ".tsv"),
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

double_check <- double_check %>%
    filter(! is.na (c19_first_reg) )

stopped$nct_id
double_check$nct_id

double_check$already_checked <- double_check$nct_id %in% stopped$nct_id

double_check %>%
    filter (! already_checked) %>%
    select (nct_id, c19_first_reg) %>%
    write_csv("2020-11-04-double-check.csv")

### time from trial start to stop

stopped$days_to_stop <- stopped$date_stopped - stopped$c19_first_reg
stopped_accru_coord$days_to_stop <- stopped_accru_coord$date_stopped - stopped_accru_coord$c19_first_reg

library(ggplot2)

ggplot (
    aes(
        x = days_to_stop/7,
        fill = c19_accru_coord_stop
    ),
    data = stopped
) +
    geom_histogram (
        binwidth=28/7
    ) +
    scale_y_continuous (
        breaks = seq(from=0, to=50, by=5),
        minor_breaks=seq(from=0, to=50, by=1)
    ) +
    scale_x_continuous (
        breaks = seq(from=0, to=50, by=4)
    ) +
    labs (
        x = "Time from first registration until trial termination, suspension or withdrawal (weeks)",
        y = "Count",
        title = "Time from first registration on ClinicalTrials.gov to termination, suspension or withdrawal for clinical trials of Covid-19"
    )
    
### the good plot

stopped <- stopped %>%
    arrange(desc(c19_first_reg))

stopped$y_coordinate <- -1 * (1 - 1:nrow(stopped))

time_from_reg_to_stop <- ggplot (
    aes(
        x = c19_first_reg,
        xend = date_stopped,
        y = y_coordinate,
        yend = y_coordinate,
        linetype = as.factor(c19_accru_coord_stop)
    ),
    data = stopped
) +
    geom_segment () +
    geom_point (
        x = stopped$c19_first_reg,
        y = stopped$y_coordinate
    ) +
    scale_y_discrete (
        limits=stopped$y_coordinate,
        labels=stopped$nct_id
    ) +
    scale_x_date (
        breaks = "1 month",
        minor_breaks = NULL
    ) +
    labs (
        x = "Time (months)",
        y = "Clinical trial identifier",
        title = "Time from first registration to termination, suspension or withdrawal",
        linetype = "Stopped due to accrual or coordination"
    ) +
    scale_linetype_manual (
        values = c("dashed", "solid"),
        labels = c("No", "Yes")
    ) +
    theme(legend.position="bottom")

pdf(
    "time-from-registration-to-stopping.pdf",
    width=16,
    height=12
)
time_from_reg_to_stop
dev.off()

png(
    "time-from-registration-to-stopping.png",
    width=1600,
    height=1200
)
time_from_reg_to_stop
dev.off()

### Now sorted by end date

stopped <- stopped %>%
    arrange(desc(date_stopped))

stopped$y_coordinate <- -1 * (1 - 1:nrow(stopped))

time_from_reg_to_stop <- ggplot (
    aes(
        x = c19_first_reg,
        xend = date_stopped,
        y = y_coordinate,
        yend = y_coordinate,
        linetype = as.factor(c19_accru_coord_stop)
    ),
    data = stopped
) +
    geom_segment () +
    geom_point (
        x = stopped$c19_first_reg,
        y = stopped$y_coordinate
    ) +
    scale_y_discrete (
        limits=stopped$y_coordinate,
        labels=stopped$nct_id
    ) +
    scale_x_date (
        breaks = "1 month"
    ) +
    labs (
        x = "Time (months)",
        y = "Clinical trial identifier",
        title = "Time from first registration on ClinicalTrials.gov to termination, suspension or withdrawal for clinical trials of Covid-19",
        linetype = "Stopped due to accrual or coordination"
    ) +
    scale_linetype_manual (
        values = c("dashed", "solid"),
        labels = c("No", "Yes")
    )

pdf(
    "time-from-registration-to-stopping-sorted-by-stopped.pdf",
    width=16,
    height=12
)
time_from_reg_to_stop
dev.off()

png(
    "time-from-registration-to-stopping-sorted-by-stopped.png",
    width=1600,
    height=1200
)
time_from_reg_to_stop
dev.off()

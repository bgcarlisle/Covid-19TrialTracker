<?php

// To monitor while it's going:
// SELECT * FROM `denominator_trials` WHERE `include` = 1 AND `c19_indication` IS NOT NULL

include_once ("../config.php");

$trials = get_included_c19arm_trials_with_indications_not_specified (4);

foreach ( $trials as $trial ) {

    echo $trial['nct_id'] . "<br>\n";

    $oncology = NULL;
    $neuro = NULL;
    $cv = NULL;
    $pain = NULL;
    $c19 = NULL;

    if ( nct_in_indication ($trial['nct_id'], "oncology") ) {
        $oncology = TRUE;
        echo "Cancer ";
    } else {
        $oncology = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "neurologic") ) {
        $neuro = TRUE;
        echo "Neuro ";
    } else {
        $neuro = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "cardiovascular") ) {
        $cv = TRUE;
        echo "CV ";
    } else {
        $cv = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "pain") ) {
        $pain = TRUE;
        echo "Pain ";
    } else {
        $pain = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "COVID-19") ) {
        $c19 = TRUE;
        echo "C19 ";
    } else {
        $c19 = FALSE;
    }

    echo "<br>\n";

    if ( ! is_null ($oncology) & ! is_null ($neuro) & ! is_null ($cv) & ! is_null ($pain) & ! is_null($c19) ) {
        if (update_denominator_indications ($trial['id'], $oncology, $neuro, $cv, $pain, $c19)) {
            echo "Updated database<br><br>\n\n";
        } else {
            echo "Database error<br><br>\n";
        }
    }

}

if ( count ($trials) > 0 ) {
    ?><script>
    setTimeout(function () {
        location.reload();
    }, 0); // Set this to a number of thousandths of a second to wait between refreshes
</script><?php
}

?>

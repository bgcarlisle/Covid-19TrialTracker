---
title: "Clinical trials that were terminated, suspended or withdrawn due to Covid-19: The hidden costs to human research of a global pandemic"
author: "Benjamin Gregory Carlisle PhD"
date: "2020-03-31"
---

# Background

The Covid-19 outbreak that became a global pandemic in 2020 has disrupted people's lives, families, industries and entire nations.
This project aims to record the effect of Covid-19 on the enterprise of human research.

# Primary objective

The primary objective of this study is to measure the effect of the Covid-19 pandemic on the conduct of registered clinical trials, in terms of terminations, suspensions and withdrawals of ClinicalTrials.gov registry entries explicitly attributed to Covid-19.
We will quantify the number of clinical trials that were stopped because of Covid-19, and the number of patient-subjects these trials included or would have included.
This will be compared against the set of clinical trials that were terminated, suspended or withdrawn during a timeframe of the same length, starting two years earlier, to allow for at least one year of follow-up.

# Secondary objectives

1. To quantify the total number of terminations, suspensions and withdrawals since 2019-12-01, regardless of cause.
2. To assess the effect of Covid-19 on subgroups of clinical trials defined by broad indication areas (e.g. trials with cancer indications vs trials with cardiovascular indications).
3. To assess the dates when clinical trials were terminated, suspended or withdrawn because of Covid-19.
4. To assess the locations of clinical trials that were terminated, suspended or withdrawn because of Covid-19.
5. To assess new trial registrations that took place during the same time period.
6. To calculate the rate with which trials that are suspended, terminated or withdrawn are re-started within a year, in a comparator sample of trials taken from a timeframe of equal length to the original sample, starting 2017-12-01

# Hypothesis

We hypothesize that there will be a higher volume of stopped clinical trials due to Covid-19 than in the same timeframe from the previous year.

# Methods overview

This project will proceed in three broad steps:

1. Download, screen and extract clinical trial registry entries that terminated, suspended or were withdrawn, starting 2019-12-01, and identify whether they were stopped due to Covid-19 or not
2. Download, screen and extract clinical trial registry entries that terminated, suspended or were withdrawn, for a matched time period, starting 2017-12-01
3. Download, screen and extract clinical trial registry entries that were first registered starting 2019-12-01, and identify whether they were initiated due to Covid-19 or not

## Sampling methods

The initial sample will be populated by downloading all trials on ClinicalTrials.gov with an overall status of: Terminated, Suspended or Withdrawn and a last update posted after 2019-12-01.
Sampling will continue daily using a web app until 2020-11-30 (1 year after beginning of trial sample), or until there has been 14 days without a clinical trial that has had its overall status changed to Terminated, Suspended or Withdrawn with Covid-19 as an explicitly stated reason.

Because there is no way to directly search for clinical trials whose status changed to Terminated, Suspended or Withdrawn within a certain time period, we will download all the registry entries and then use a web-scraper to automate the task of excluding clinical trials that were already stopped before 2019-12-01.

Similar methods will be applied to the matched sample starting at 2018-12-01.

## Trial screening

Trials will be included in the sample if:

* They are captured in the above search
* The overall status of the trial changed from any other status to Terminated, Suspended or Withdrawn after 2019-12-01

This will allow us to address secondary objective 1.

## Data collection

The "Why stopped" field of every trial will be manually screened for whether it contains an explicit mention of Covid-19.
This will include the following:

* Covid-19
* COVID19
* Covid
* NCoV-2019
* Coronavirus
* Corona virus
* Corona
* The current pandemic
* Institution suspended all clinical research not deemed medically essential

## Sample size rationale and calculation

We will collect all trials that meet the inclusion criteria above.

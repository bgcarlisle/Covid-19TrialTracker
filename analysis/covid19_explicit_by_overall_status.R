library(readr)
library(ggplot2)

covid_trials <- read_delim(
  "covid_export.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE
)

summary(as.factor(covid_trials$covid19_explicit))

png(
  "covid19_explicit_by_overall_status.png",
  800,
  500
)

ggplot(
  aes(
    x = as.Date(date_stopped),
    fill = overall_status
  ),
  data = subset(covid_trials, covid19_explicit == 1)
) + geom_histogram(
  binwidth = 1
) + scale_x_date(
  date_breaks = "1 week",
  date_minor_breaks = "1 day"
) + scale_y_continuous(
  breaks = seq(0, 100, 10),
  minor_breaks = seq(0, 100, 1)
) + labs(
  x = "Date that clinical trial was stopped",
  y = "Number of clinical trials",
  fill = "Status",
  title = "Clinical trials that were terminated, suspended or withdrawn due to Covid-19"
) + scale_fill_manual (
  values = c(
    "#ffc107",
    "#dc3545",
    "#17a2b8"
  )
)

dev.off()
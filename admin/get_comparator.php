<?php

// Go to the address for this PHP file in your browser and add the following:
// ?start=2018-12-01&end=2018-12-02
// And it will start working

include_once ("../config.php");

$days_at_a_time = 2;

echo "https://clinicaltrials.gov/ct2/results/download_studies?lupd_s=" . date("m/d/Y", strtotime($_GET['start'])) . "&lupd_e=" . date("m/d/Y", strtotime($_GET['end'])) . "<br><br>";

if (comparator_download ($_GET['start'], $_GET['end'])) {

    echo "Success<br><br>";

    echo "Next dates: " . date("Y-m-d", strtotime($_GET['end']) + 86400) . " to " . date("Y-m-d", strtotime($_GET['end']) + 86400 * $days_at_a_time);

    if (strtotime($_GET['end']) < strtotime("2020-04-30")) {

?><script>

   setTimeout(function () {
       window.location = '<?php echo SITE_URL; ?>admin/get_comparator.php?start=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400) ?>&end=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400*$days_at_a_time) ?>';
   }, 3000);
   
</script><?php

	 }

	 } else {
	     echo "Something went wrong";

	 ?><script>
	    setTimeout(function () {
		window.location = '<?php echo SITE_URL; ?>admin/get_comparator.php?start=<?php echo $_GET['start']; ?>&end=<?php echo date("Y-m-d", strtotime($_GET['end']) + 86400); ?>';
	    }, 1000);
	 </script><?php
	 }

	 ?>

<?php

include_once ("../config.php");

$nct_id = $_GET['nct_id'];
$nct_id = $_POST['nctid'];
$date_of_interest = "2019-12-01";

$ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;
$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

if ($remote_index_file) {

    // Downloaded the HTML for the index of previous versions

    // This extracts a date for each of the versions of the registry entry
    preg_match_all (
	'/>([A-Za-z0-9, ]+)<\/a>/',
	$remote_index_file,
	$dates,
	PREG_UNMATCHED_AS_NULL
    );

    $found_stopped_after_date_of_interest = FALSE;
    $last_version_before_date_of_interest = FALSE;
    $printed_rule_at_date_of_interest = FALSE;
    
    // This loops through each extracted date
    foreach ($dates[1] as $version_index=>$version_date) {

	$version_index = $version_index+1;

	if (strtotime($version_date) < strtotime($date_of_interest)) { // If this version is before the date of interest

	    // echo $version_date . "<br>";
	    
	    // Do this until the first one after the date of interest
	    $last_version_before_date_of_interest = "https://clinicaltrials.gov/ct2/history/" . $nct_id . "?V_" . $version_index . "=View#StudyPageTop";
	    $last_before_date = $version_date;

	} else { // If this version is on or after the date of interest

	    if ( ! $printed_rule_at_date_of_interest ) {
		// echo "<hr>";
		$printed_rule_at_date_of_interest = TRUE;
	    }

	    // Only check versions until one is found that has been stopped, then stop checking
	    if ( ! $found_stopped_after_date_of_interest ) {

		$version_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id . "?V_" . $version_index . "=View#StudyPageTop";
		
		$remote_version_file = file_get_contents($version_url, FALSE, $context);

		if ($remote_version_file) {
		    
		    $processed_html = str_replace("\n", "", $remote_version_file);
		    $processed_html = str_replace(" ", "", $processed_html);

		    $matching = preg_match (
			'/(>Terminated<\/|>Suspended<\/|>Withdrawn<\/)/',
			$processed_html,
			$match,
			PREG_UNMATCHED_AS_NULL
		    );

		    if ($matching == 1) {
			
			$found_stopped_after_date_of_interest = TRUE;

			$include_date = date("Y-m-d", strtotime($version_date));

			// echo "> ";

			$first_after = $match[0];
			
		    }
		    
		}
		
	    }

	    // echo $version_date . "<br>";
	}
	
    }
    
} else {
    echo "<p>Could not load the index file for " . $nct_id . "</p>";
}

// echo "<hr>";

// Check the last version before the date of interest

$version_url = $last_version_before_date_of_interest;

$remote_version_file = file_get_contents($version_url, FALSE, $context);

if ($remote_version_file) {
    
    $processed_html = str_replace("\n", "", $remote_version_file);
    $processed_html = str_replace(" ", "", $processed_html);

    $matching = preg_match (
	'/(>Terminated<\/|>Suspended<\/|>Withdrawn<\/)/',
	$processed_html,
	$match,
	PREG_UNMATCHED_AS_NULL
    );

    if ($matching == 1) {
	
	// echo "last before: " . str_replace("</", "", str_replace(">", "", $match[0])) . "<br>";

	$stopped_before = TRUE;
	
    } else {

	// echo "last before: not stopped<br>";

	$stopped_before = FALSE;

    }
    
} else {

    echo "Could not load " . $version_url . "<br>";
}

// Final evaluation to see if it should be included

if ($found_stopped_after_date_of_interest) {

    // echo "stopped after<br>";

    if (! $stopped_before) {

	echo $include_date;
	
    } else {

	echo "Exclude";
    }
    
} else {

    echo "Exclude";
    
}

?>

library(tidyverse)
library(ggplot2)

## Read the covid arm TSV
c19trials <- read_delim(
  "../data/covid-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Read the comparator arm TSV
comptrials <- read_delim(
  "../data/comparator-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Get rid of that outlier
cutoff <- 5000
c19trials <- c19trials %>%
    filter(enrollment < cutoff)
comptrials <- comptrials %>%
    filter(enrollment < cutoff)

## Only look at interventional trials
c19trials <- c19trials %>%
    filter(study_type == "Interventional")

comptrials <- comptrials %>%
    filter(study_type == "Interventional")

## Remove data points from outside 1-year sample
c19trials <- c19trials %>%
    filter(date_stopped < as.Date("2020-12-01"))

## Sort by date
c19trials <- c19trials %>%
    arrange(date_stopped)

comptrials <- comptrials %>%
    arrange(date_stopped)

## Only consider trials with an actual enrolment
## greater than zero
c19trials <- c19trials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

comptrials <- comptrials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

## Move the dates in the comparator forward

comptrials$date_stopped <- comptrials$date_stopped + 365*2

## Make a subset that includes only trials stopped for reasons
## that explicitly cite Covid-19
c19e <- c19trials %>%
    filter(covid19_explicit == 1)

## Generate plot
fig4 <- ggplot(
    aes(
        x = date_stopped,
        y = enrollment,
        color = "Stopped during Covid-19 pandemic (Dec 2019 to Nov 2020, any reason)"
    ),
    data = c19trials
) +
    geom_point() +
    scale_x_date(
        breaks = "1 month",
        minor_breaks = NULL,
        date_labels = "%b"
    ) +
    labs(
        x = "Date trial stopped",
        y = "Cumulative number of enrolled patients",
        linetype = "Arm"
    ) +
    geom_point(
        aes(
            x = date_stopped,
            y = enrollment,
            color = "Stopped in comparator (Dec 2017 to Nov 2018, any reason)"
        ),
        data = comptrials
    ) +
    geom_point(
        aes(
            x = date_stopped,
            y = enrollment,
            color = "Stopped for reasons citing Covid-19 (Dec 2019 to Nov 2020)"
        ),
        data = c19e
    ) +
    scale_color_manual(
        breaks = c(
            "Stopped during Covid-19 pandemic (Dec 2019 to Nov 2020, any reason)",
            "Stopped for reasons citing Covid-19 (Dec 2019 to Nov 2020)",
            "Stopped in comparator (Dec 2017 to Nov 2018, any reason)"
        ),
        values = c(1,2,3)
    ) +
    theme(
        legend.position="bottom",
        legend.direction="vertical"
    )

fig4

## medians

c19e$enrollment %>% median()

comptrials$enrollment %>% median()

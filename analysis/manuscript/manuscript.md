---
title: "A review of clinical trial closures during the first twelve months of the Covid-19 pandemic"
author: "Benjamin Gregory Carlisle PhD"
csl: nature.csl
bibliography: Stopped.bib
date: "2021-03-03"
---

**Abstract**

# Introduction

# Methods

All clinical trial registry records from ClinicalTrials.gov that were updated after 2019-12-01 were downloaded into a database that was updated daily.
Clinical trial records whose overall status changed to "Terminated", Suspended" or "Withdrawn" after 2019-12-01 were included.
Clinical trials with an overall status that was already "Terminated", "Suspended" or "Withdrawn" before 2019-21-01 were excluded.
The "why stopped" field for every trial record was manually screened for whether it indicated explicitly that the trial was stopped due to Covid-19, including ters such as "Covid-19", "COVID19", "Covid", "NCoV-2019", "Coronavirus", "Corona virus", "Corona", etc.
The complete list of "why stopped" field entries that were included are available from the Open Science Foundation.[@C19Stopped-Project]

Other clinical trial registry data were automatically downloaded from the ClinicalTrials.gov XML files, such as title, summary and detailed description, primary completion date, actual or anticipated enrollment, phase number, study type, patient allocation, intervention model, primary purpose, masking, indications, interventions and sponsors.
Historical versions of clinical trial registry entries were also downloaded by web-scraping ClinicalTrials.gov.

A matching cohort of clinical trial registry data were downloaded from the same months, two years earlier, from 2017-12-01 to 2018-11-30, to serve as a comparison baseline.

Trial data were downloaded from ClinicalTrials.gov using a web app written for this project.[@CarlisleBenjaminGregoryCovid19TrialTracker]
Historical versions of clinical trial records were downloaded using Clinical Trials History Scraper (RRID:SCR_019229).[@CarlisleClinicalTrialsHistoryScraper]
Analysis of data was performed using *R* v. 3.6.3.[@R]

The protocol for this project was preregistered with the Open Science Foundation before data collection.[@C19Stopped-Protocol]
The data[@C19Stopped-Project] and code[@CarlisleBenjaminGregoryCovid19TrialTracker] for this project are available online.

All statistical tests were two-sided, defining $p<0.05$ to be statistically significant.
A $z$-test was used to calculate test statistics for proportions of trials between samples.
Statistical tests are exploratory only, and there was no correction for multiple testing.

# Results



# Discussion

# Figures and tables

# References

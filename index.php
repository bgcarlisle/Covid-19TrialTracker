<?php

include_once ("config.php");

$full_report_pdf_files = glob ( ABS_PATH . "published/full-report-*.pdf");

$full_report_pdf = substr ( $full_report_pdf_files[count($full_report_pdf_files)-1], strrpos ( $full_report_pdf_files[count($full_report_pdf_files)-1], "/" ) + 1 );

$full_report_html_files = glob ( ABS_PATH . "published/full-report-*.html");

$full_report_html = substr ( $full_report_html_files[count($full_report_html_files)-1], strrpos ( $full_report_html_files[count($full_report_html_files)-1], "/" ) + 1 );

$c19_arm_files = glob ( ABS_PATH . "published/covid-arm-*.tsv");

$c19_arm_tsv = substr ( $c19_arm_files[count($c19_arm_files)-1], strrpos ( $c19_arm_files[count($c19_arm_files)-1], "/" ) + 1 );

$comp_arm_files = glob ( ABS_PATH . "published/comparator-arm-*.tsv");

$comp_arm_tsv = substr ( $comp_arm_files[count($comp_arm_files)-1], strrpos ( $comp_arm_files[count($comp_arm_files)-1], "/" ) + 1 );

$druglist_files = glob ( ABS_PATH . "published/druglist-*.tsv");

$druglist_tsv = substr ( $druglist_files[count($druglist_files)-1], strrpos ( $druglist_files[count($druglist_files)-1], "/" ) + 1 );

$png_files = glob ( ABS_PATH . "published/figures-png-*.zip");

$png = substr ( $png_files[count($png_files)-1], strrpos ( $png_files[count($png_files)-1], "/" ) + 1 );

$pdf_files = glob ( ABS_PATH . "published/figures-pdf-*.zip");

$pdf = substr ( $pdf_files[count($pdf_files)-1], strrpos ( $pdf_files[count($pdf_files)-1], "/" ) + 1 );

$img_files = glob ( ABS_PATH . "img/*-Figure-1.png");

$img = substr ( $img_files[count($img_files)-1], strrpos ( $img_files[count($img_files)-1], "/" ) + 1 );

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Clinical trials stopped by Covid-19</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- Stopped Covid-19 trials CSS -->
	<link href="<?php echo SITE_URL; ?>covid-19.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<nav class="nav">
	    <!--<a class="nav-link" href="https://blog.bgcarlisle.com/covid-19/" target="_blank">About</a>-->
	    <a class="nav-link" href="<?php echo SITE_URL; ?>published/<?php echo $full_report_html; ?>" target="_blank">Full report (draft)</a>
	    <a class="nav-link" href="#" onclick="show_citation(event);">Cite</a>
	    <a class="nav-link" href="https://doi.org/10.17605/OSF.IO/PRAFD" target="_blank">Protocol</a>
	    <a class="nav-link" href="https://codeberg.org/bgcarlisle/Covid-19TrialTracker" target="_blank">Source</a>
	    <a class="nav-link" href="https://scholar.social/@bgcarlisle" target="_blank">Social media</a>
	    <a class="nav-link" href="#" onclick="show_contact(event);">Contact</a>	    
	</nav>
	<div id="citeMask" class="hidden" onclick="hide_dialog (event);">&nbsp;</div>
	<div id="contact" class="hidden">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Hide contact</button>
	    <h2>Contact</h2>
	    <div class="form-group">
		<label for="contact-email">Your email address</label>
		<input type="text" class="form-control" id="contact-email" name="contact-email" aria-describedby="emailHelp">
		<small id="emailHelp" class="form-text text-muted">If you don't give me your email or if it's typed wrong, I can't reply</small>
	    </div>
	    <div class="form-group">
		<label for="contact-message">Message</label>
		<textarea class="form-control" id="contact-message" name="contact-message" rows="5"></textarea>
		<small id="notesHelp" class="form-text text-muted"></small>
	    </div>
	    <button class="btn btn-block btn-primary" style="margin-bottom: 40px;" onclick="send_message(event);">Send</button>
	</div>
	<div id="citeC19" class="hidden">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Hide citation info</button>
	    <h2>How to cite Clinical trials stopped by Covid-19</h2>
	    <h3>BibTeX</h3>
	    <pre>
@software{carlisle_covid19_2020,
location = {{Retrieved from https://covid19.bgcarlisle.com/}},
title = {Clinical trials stopped by Covid-19},
url = {https://covid19.bgcarlisle.com/},
organization = {{The Grey Literature}},
date = {2020-03},
author = {Carlisle, Benjamin Gregory}
}
	    </pre>
	    <h3>Vancouver</h3>
	    <p>Carlisle BG. Clinical trials stopped by Covid-19 [Internet]. Retrieved from https://covid19.bgcarlisle.com/: The Grey Literature; 2020. Available from: https://covid.bgcarlisle.com/</p>
	    <h3>AMA</h3>
	    <p>Carlisle BG. <i>Clinical trials stopped by Covid-19</i>. Retrieved from https://covid19.bgcarlisle.com/: The Grey Literature; 2020. https://covid19.bgcarlisle.com/.</p>
            <h3>MLA</h3>
            <p>Carlisle, Benjamin Gregory. <i>Clinical trials stopped by Covid-19.</i> The Grey Literature, 2020, https://covid19.bgcarlisle.com/.</p>
        </div>
	<div id="trialDetails" class="hidden">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Hide details</button>
	    <h2><span class="ctdetail" id="nct_id"></span></h2>
	    <p>Brief title: <span class="ctdetail" id="brief_title"></span></p>
	    <p>Official title: <span class="ctdetail" id="official_title"></span></p>
	    <p>Indications: <span class="ctdetail" id="indications"></span></p>
	    <p>Overall status: <span class="ctdetail" id="overall_status"></span> <span class="ctdetail" id="why_stopped"></span></p>
	    <p>Phase: <span class="ctdetail" id="phase"></span></p>
	    <p>Study type: <span class="ctdetail" id="study_type"></span></p>
	    <p>Allocation: <span class="ctdetail" id="allocation"></span></p>
	    <p>Enrollment: <span class="ctdetail" id="enrollment"></span></p>
	    
	    <h3>Interventions</h3>
	    <table class="table table-hover table-sm" id="interventions-table">
		<thead class="thead-light">
		    <tr>
			<th scope="col">Name</th>
			<th scope="col">Type</th>
		    </tr>
		</thead>
	    </table>
	    
	    <h3>Sponsors</h3>
	    <table class="table table-hover table-sm" id="sponsor-table">
		<thead class="thead-light">
		    <tr>
			<th scope="col">Agency</th>
			<th scope="col">Agency class</th>
		    </tr>
		</thead>
	    </table>

	    <h3>Brief summary</h3>
	    <p class="ctdetail" id="brief_summary"></p>

	    <h3>Detailed description</h3>
	    <p class="ctdetail" id="detailed_description"></p>
	    
	</div>

	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>Clinical trials stopped by Covid-19</h1>
		    <p style="font-style: italic;">Benjamin Gregory Carlisle PhD</p>
		    <p style="font-style: italic; color: #666;">Charité – Universitätsmedizin Berlin, Germany<br>Berliner Institut für Gesundheitsforschung (BIG) / Berlin Institute of Health (BIH)<br>QUEST – Quality | Ethics | Open Science | Translation</p>
		    <img src="cc-by-nc.png" title="This license lets others remix, adapt, and build upon this work non-commercially, and although their new works must also acknowledge Dr Carlisle and be non-commercial, they don’t have to license their derivative works on the same terms." onclick="show_citation(event);" style="margin-bottom: 20px;">
		</div>
	    </div>
	    <div class="row">
		<div class="col-lg-6">
		    <p>The Covid-19 outbreak that became a global pandemic in 2020 has disrupted lives, families, industries and entire nations. This project is a weekly-updated "living paper" that aims to quantify some of the effects of Covid-19 on the enterprise of human research.</p>
		    <p>The following table contains all registry entries from ClinicalTrials.gov whose overall status changed to Terminated, Suspended or Withdrawn since 2019-12-01. Investigator-provided reasons that explicitly mention Covid-19 are highlighted in green. Click a row of the table for more details about the trial.</p>
		    <p>To date, <?php echo get_c19_explicit_count (); ?> clinical trials have been stopped for reasons that explicitly cite Covid-19.</p>
		    <p>These data and figures are available to be downloaded under a Creative Commons By-Attribution Non-Commercial license. This data set was last updated <?php echo substr (get_last_update(), 0, 10); ?>.</p>
		    <a href="<?php echo SITE_URL; ?>published/<?php echo $full_report_html; ?>" class="btn btn-primary btn-block" target="_blank">View full report (draft)</a>
		    <div class="table-responsive">
			<table class="table table-striped table-hover table-sm" style="margin-top: 22px;">
			    <tbody>
				<tr>
				    <th scope="row">Figures</th>
				    <td style="text-align: right; min-width: 120px">
					<a href="<?php echo SITE_URL; ?>published/<?php echo $png; ?>" class="btn btn-primary btn-sm">PNG</a>
					<a href="<?php echo SITE_URL; ?>published/<?php echo $pdf; ?>" class="btn btn-primary btn-sm">PDF</a>
				    </td>
				</tr>
				<tr>
				    <th scope="row">Covid-19 arm per-trial data set</th>
				    <td style="text-align: right;">
					<a href="<?php echo SITE_URL; ?>published/<?php echo $c19_arm_tsv; ?>" class="btn btn-primary btn-sm">TSV</a>
				    </td>
				</tr>
				<tr>
				    <th scope="row">Comparator arm per-trial data set</th>
				    <td style="text-align: right;">
					<a href="<?php echo SITE_URL; ?>published/<?php echo $comp_arm_tsv; ?>" class="btn btn-primary btn-sm">TSV</a>
				    </td>
				</tr>
				<tr>
				    <th scope="row">Drugs and biologics in stopped trials</th>
				    <td style="text-align: right;">
					<a href="<?php echo SITE_URL; ?>published/<?php echo $druglist_tsv; ?>" class="btn btn-primary btn-sm">TSV</a>
				    </td>
				</tr>
			    </tbody>
			</table>
		    </div>
		</div>
		<div class="col-lg-6">
		    <img src="<?php echo SITE_URL; ?>img/<?php echo $img; ?>" style="width: 100%;">
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <div class="table-responsive">
			<table class="table table-striped table-hover table-sm">
			    <thead class="thead-light">
				<tr>
				    <th scope="col">NCT ID</th>
				    <th scope="col">Overall status</th>
				    <th scope="col">Brief title</th>
				    <th scope="col">Phase</th>
				    <th scope="col">Study type</th>
				    <th scope="col">Why stopped</th>
				    <th scope="col" style="text-align: right;">Enrollment</th>
				</tr>
			    </thead>
			    <tbody>
				<?php

				$rows = get_display_table_entries ();

				foreach ( $rows as $row ) {

				    switch ($row['overall_status']) {
					case "Terminated":
					    $status_css = "danger";
					    break;
					case "Suspended":
					    $status_css = "warning";
					    break;
					case "Withdrawn":
					    $status_css = "info";
					    break;
				    }

				    if ($row['covid19_explicit'] == 1) {
					$row_css = " class=\"table-success\"";
				    } else {
					$row_css = "";
				    }

				?><tr onclick="show_trial_details(<?php echo $row['id']; ?>);">
				<th scope="row"><a href="https://clinicaltrials.gov/ct2/show/<?php echo $row['nct_id']; ?>" target="_blank"><?php echo $row['nct_id']; ?></a></th>
				<td><span class="badge badge-<?php echo $status_css; ?>"><?php echo $row['overall_status']; ?></span></td>
				<td><?php echo $row['brief_title']; ?></td>
				<td><?php echo $row['phase']; ?></td>
				<td><?php echo $row['study_type']; ?></td>
				<td<?php echo $row_css; ?>><?php echo $row['why_stopped']; ?></td>
				<td style="text-align: right;"><?php if ( ! is_null ($row['enrollment_type']) ) { ?><span class="badge badge-primary" style="float: left;"><?php echo $row['enrollment_type']; ?></span> <?php } ?><?php echo $row['enrollment']; ?></td>
				</tr><?php
				     
				     }
				     
				     ?>
			    </tbody>
			</table>
		    </div>
		</div>
	    </div>
	</div>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- Stopped Covid-19 trials JS -->
	<script>
	 function show_citation (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#citeC19').slideDown();
	     });
	 }

	 function show_contact (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#contact').slideDown();
	     });
	 }

	 function hide_dialog (event) {
	     event.preventDefault();
	     $('#citeC19,#trialDetails,#contact').slideUp(0, function () {
		 $('#citeMask').fadeOut();
		 $('.ctdetail').html('');
		 $('.ctdetail-row').remove();
	     });
	 }

	 function send_message (event) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'send-message.php',
		 type: 'post',
		 data: {
		     email: $('#contact-email').val(),
		     message: $('#contact-message').val()
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if (response == "Sent") {
		     $('#contact').html('<div class="alert alert-success" role="alert">Message sent</div>');

		     setTimeout ( function () {
			 hide_dialog(event);
		     }, 5000);
		 } else {
		 }
	     });
	     
	 }

	 function show_trial_details (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'trial_details.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (json_response) {

		 response = JSON.parse(json_response);

		 // Trial details

		 $('#nct_id').html(response[0].nct_id);
		 $('#brief_title').html(response[0].brief_title);
		 $('#official_title').html(response[0].official_title);
		 $('#overall_status').html(response[0].overall_status);
		 $('#why_stopped').html('(' + response[0].why_stopped.trim() + ')');
		 $('#phase').html(response[0].phase);
		 $('#allocation').html(response[0].allocation);
		 $('#study_type').html(response[0].study_type);
		 $('#enrollment').html(response[0].enrollment + ' <span class="badge badge-primary">' + response[0].enrollment_type + '<span>');
		 
		 $('#brief_summary').html(response[0].brief_summary.trim().replace('\n\n', '</p><p>'));
		 $('#detailed_description').html(response[0].detailed_description.trim().replace('\n\n', '</p><p>'));

		 // Sponsors

		 for (var i = 0; i < response[1].length; i++) {

		     sponsor = response[1][i];

		     if (sponsor.lead_or_collaborator == "lead_sponsor") {
			 sponsor_badge = '<span class="badge badge-primary">Lead</span>';
		     } else {
			 sponsor_badge = '<span class="badge badge-secondary">Collaborator</span>';
		     }

		     $('#sponsor-table').append('<tr class="ctdetail-row"><td>' + sponsor.agency + ' ' + sponsor_badge + '</td><td>' + sponsor.agency_class + '</td></tr>');
		     
		 }

		 // Interventions

		 for (var i = 0; i < response[2].length; i++) {

		     intervention = response[2][i];

		     $('#interventions-table').append('<tr class="ctdetail-row"><td title="' + intervention.description + '">' + intervention.intervention_name + '</td><td>' + intervention.intervention_type + '</td></tr>');
		 }

		 // Indications

		 for (var i = 0; i < response[3].length; i++) {

		     indication = response[3][i];

		     $('#indications').append('<span class="badge badge-secondary" style="margin-right: 4px;">' + indication.indication + '</span>');
		 }

	     });
	     
	     $('#citeMask').fadeIn(400, function () {
		 $('#trialDetails').slideDown();
	     });
	 }
	</script>
    </body>
</html>

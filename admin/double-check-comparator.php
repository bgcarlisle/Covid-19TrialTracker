<?php

include_once ("../config.php");

$nct_id = $_GET['nct_id'];
$date_of_interest = "2018-12-01";

$ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id;
$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

if ( $remote_index_file ) {

    echo "Loaded index<br><br>\n\n";

    // This extracts a date for each of the versions of the registry entry
    $regex = '/>([A-Za-z0-9, ]+)<\/a>/';
    
    preg_match_all (
	$regex,
	$remote_index_file,
	$dates,
	PREG_UNMATCHED_AS_NULL
    );

    $sections = preg_split(
	$regex,
	$remote_index_file,
	-1, // no limit
	PREG_SPLIT_NO_EMPTY
    );

    foreach ($dates[1] as $version_index=>$version_date) {

	// echo $version_date;

	$version_index = $version_index++;

	if ( strtotime($version_date) > strtotime($date_of_interest) ) {
	    // If this version is after the date of interest ...

	    // echo "After<br>\n";

	    // echo "<pre>" . $sections[$version_index+1] . "</pre>";

	    if ( strpos($sections[$version_index+1], " --> Suspended") | strpos($sections[$version_index+1], " --> Terminated") | strpos($sections[$version_index+1], " --> Withdrawn") ) {

		echo "Stopped on " . $version_date . "<br>\n";
		
	    }
	}
    }
    
} else {
    echo "<p>Could not load the index file for " . $nct_id . "</p>";
}

?>

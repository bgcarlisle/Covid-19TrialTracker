<?php

include_once ("../config.php");

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Clinical trials stopped by Covid-19</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- Stopped Covid-19 trials CSS -->
	<link href="<?php echo SITE_URL; ?>covid-19.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>Admin: Comparator arm</h1>

		    <p>Remaining to be screened: <?php echo count_comparator_trials_not_assessed_for_inclusion(); ?></p>

		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <th scope="col">NCT ID</th>
			    <th scope="col">Archives</th>
			    <th scope="col">Inclusion</th>
			    <th scope="col">Last update</th>
			    <th scope="col">Include</th>
			    <th scope="col">Date stopped</th>
			    <th scope="col">Status when stopped</th>
			    <!-- <th scope="col">Restart</th>-->
			    <th scope="col">Restarted</th>
			    <th scope="col">Restart date</th>
			</thead>
			<tbody>
			    <?php

			    $rows = get_admin_comparator_table_entries ();

			    foreach ( $rows as $row ) {

				switch ($row['overall_status']) {
				    case "Terminated":
					$row_css = "danger";
					break;
				    case "Suspended":
					$row_css = "warning";
					break;
				    case "Withdrawn":
					$row_css = "info";
					break;
				}

			    ?><tr>
			    <th scope="row"><a href="https://clinicaltrials.gov/ct2/show/<?php echo $row['nct_id']; ?>" target="_blank"><?php echo $row['nct_id']; ?></a></th>
			    <td><a href="https://clinicaltrials.gov/ct2/history/<?php echo $row['nct_id']; ?>" target="_blank">Changes</a></td>
			    <td><button onclick="auto_check_comparator_inclusion (<?php echo $row['id']; ?>, '<?php echo $row['nct_id']; ?>');" class="btn btn-sm btn-primary autocheck-include" id="acbutton-<?php echo $row['id']; ?>">Autocheck</button></td>
			    <td onclick="$('#datestopped-<?php echo $row['id']; ?>').val('<?php echo $row['last_update_submitted']; ?>').trigger('input');"><?php echo $row['last_update_submitted']; ?></td>
			    <?php

			    // Include row

			    if ( $row['include'] === NULL ) {
				echo "<td id=\"include-" . $row['id'] . "\" onclick=\"click_include(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['include']) {
				    case 0:
					echo "<td id=\"include-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_include(" . $row['id'] . ");\">No</td>";
					break;
				    case 1:
					echo "<td id=\"include-" . $row['id'] . "\" class=\"table-success\" onclick=\"click_include(" . $row['id'] . ");\">Yes</td>";
					break;
				}
			    }
			    
			    ?>
			    <td>
				<div class="input-group">
				    <div class="input-group-prepend">
					<span class="input-group-text" id="date_prepend">YYYY-MM-DD</span>
				    </div>
				    <input type="text" id="datestopped-<?php echo $row['id']; ?>" trial_id="<?php echo $row['id']; ?>" class="form-control date-stopped" placeholder="-" aria-label="Date" aria-describedby="date_prepend" value="<?php echo $row['date_stopped']; ?>">
				</div>
			    </td>
			    <?php

			    // Status when stopped row

			    if ( $row['status_when_stopped'] === NULL ) {
				echo "<td id=\"statusws-" . $row['id'] . "\" onclick=\"click_statusws(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['status_when_stopped']) {
				    case "S":
					echo "<td id=\"statusws-" . $row['id'] . "\" class=\"table-warning\" onclick=\"click_statusws(" . $row['id'] . ");\">Suspended</td>";
					break;
				    case "T":
					echo "<td id=\"statusws-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_statusws(" . $row['id'] . ");\">Terminated</td>";
					break;
				    case "W":
					echo "<td id=\"statusws-" . $row['id'] . "\" class=\"table-info\" onclick=\"click_statusws(" . $row['id'] . ");\">Withdrawn</td>";
					break;
				}
			    }

			    ?>
			    <!-- <td><button onclick="auto_check_comparator_restart (<?php echo $row['id']; ?>, '<?php echo $row['nct_id']; ?>');" class="btn btn-sm btn-primary">Autocheck</button></td> -->
			    <?php

			    
			    
			    // Restarted row
			    
			    if ( $row['restarted6'] === NULL ) {
				echo "<td id=\"restarted6-" . $row['id'] . "\" onclick=\"click_restarted6(" . $row['id'] . ");\">-</td>";
			    } else {
				switch ($row['restarted6']) {
				    case 0:
					echo "<td id=\"restarted6-" . $row['id'] . "\" class=\"table-danger\" onclick=\"click_restarted6(" . $row['id'] . ");\">No</td>";
					break;
				    case 1:
					echo "<td id=\"restarted6-" . $row['id'] . "\" class=\"table-success\" onclick=\"click_restarted6(" . $row['id'] . ");\">Yes</td>";
					break;
				}
			    }
			    
			    ?>
			    <td>
				<div class="input-group">
				    <div class="input-group-prepend">
					<span class="input-group-text" id="restartdate_prepend">YYYY-MM-DD</span>
				    </div>
				    <input type="text" id="restartdate-<?php echo $row['id']; ?>" trial_id="<?php echo $row['id']; ?>" class="form-control date-restarted" placeholder="-" aria-label="Date" aria-describedby="restartdate_prepend" value="<?php echo $row['restartdate']; ?>">
				</div>
			    </td>
			    </tr><?php
				 
				 }

				 ?>
			</tbody>
		    </table>

		    <a href="<?php echo SITE_URL; ?>admin/screen_comparator.php" class="btn btn-primary btn-block" style="margin-bottom: 40px;">More</a>
		</div>
	    </div>
	</div>
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- Stopped Covid-19 trials JS -->
	<script>
	 function click_include (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/include_comparator.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error') {

		     $('#include-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#include-' + trial_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#include-' + trial_id).removeClass('table-success');
			     $('#include-' + trial_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#include-' + trial_id).removeClass('table-danger');
			     $('#include-' + trial_id).addClass('table-success');
			     break;
		     }
		 }
		 
	     });
	     
	 }

	 function set_include (trial_id, newvalue) {

	     // newvalue must be 1 or 0
	     // don't try for NULL

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/include_comparator_set.php',
		 type: 'post',
		 data: {
		     tid: trial_id,
		     nv: newvalue
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 switch (response) {
		     case '1':
			 $('#include-' + trial_id).html('Yes');
			 $('#include-' + trial_id).removeClass('table-danger');
			 $('#include-' + trial_id).addClass('table-success');
			 break;
		     case '0':
			 $('#include-' + trial_id).html('No');
			 $('#include-' + trial_id).removeClass('table-success');
			 $('#include-' + trial_id).addClass('table-danger');
			 break;
		 }
	     });
	     
	 }

	 function click_statusws (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/statusws_comparator.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error' ) {

		     $('#statusws-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#statusws-' + trial_id).removeClass('table-danger table-info table-warning');
			     break;
			 case 'Suspended':
			     $('#statusws-' + trial_id).removeClass('table-danger table-info');
			     $('#statusws-' + trial_id).addClass('table-warning');
			     break;
			 case 'Terminated':
			     $('#statusws-' + trial_id).removeClass('table-warning table-info');
			     $('#statusws-' + trial_id).addClass('table-danger');
			     break;
			 case 'Withdrawn':
			     $('#statusws-' + trial_id).removeClass('table-warning table-danger');
			     $('#statusws-' + trial_id).addClass('table-info');
			     break;
		     }
		     
		 }
		 
	     });
	 }

	 function click_restarted6 (trial_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/restarted6.php',
		 type: 'post',
		 data: {
		     tid: trial_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response != 'MySQL Error') {

		     $('#restarted6-' + trial_id).html(response);

		     switch (response) {
			 case '-':
			     $('#restarted6-' + trial_id).removeClass('table-danger table-success');
			     break;
			 case 'No':
			     $('#restarted6-' + trial_id).removeClass('table-success');
			     $('#restarted6-' + trial_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#restarted6-' + trial_id).removeClass('table-danger');
			     $('#restarted6-' + trial_id).addClass('table-success');
			     break;
		     }
		 }
		 
	     });
	     
	 }

	 function auto_check_comparator_inclusion (trial_id, nct_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/ctg_comparator_inclusion_history_check.php',
		 type: 'post',
		 data: {
		     nctid: nct_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if ( response == 'Exclude' ) {

		     set_include (trial_id, 0);

		     //$('#include-' + trial_id).click();
		     //setTimeout(function () {
			// $('#include-' + trial_id).click();
		     //}, 1000);
		     
		 } else {

		     $('#include-' + trial_id).click();
		     $('#datestopped-' + trial_id).val(response.substring(0, 10));
		     $('#datestopped-' + trial_id).trigger('input');

		     switch (response.substring(10).replace('>', '').replace('</', '').replace(' ', '')) {
			 case "Suspended":
			     $('#statusws-' + trial_id).click();
			     break;
			 case "Terminated":
			     $('#statusws-' + trial_id).click();
			     $('#statusws-' + trial_id).click();
			     break;
			 case "Withdrawn":
			     $('#statusws-' + trial_id).click();
			     $('#statusws-' + trial_id).click();
			     $('#statusws-' + trial_id).click();
			     break;
		     }
		 }
		 
	     });
	     
	 }

	 $('.date-stopped').on('input', function (e) {
	     selected_input = $(this);
	     trial_id = $(this).attr('trial_id');
	     newdate = $(this).val();

	     if (newdate == '') {
		 $.ajax ({
		     url: '<?php echo SITE_URL; ?>' + 'admin/comparator-clear-datestopped.php',
		     type: 'post',
		     data: {
			 tid: trial_id
		     },
		     dataType: 'html'
		 }).done ( function (response) {

		     if ( response == '-') {

			 selected_input.parent().parent().addClass('table-success');

			 setTimeout(
			     function () {
				 selected_input.parent().parent().removeClass('table-success');
			     },
			     1000
			 );
		     }

		 });
		 
	     }

	     if (/^([0-9]{4,})-([0-9]{2,})-([0-9]{2,})$/.test(newdate)) {
		 
		 $.ajax ({
		     url: '<?php echo SITE_URL; ?>' + 'admin/comparator-datestopped.php',
		     type: 'post',
		     data: {
			 tid: trial_id,
			 nd: newdate
		     },
		     dataType: 'html'
		 }).done ( function (response) {

		     if ( response != 'MySQL Error') {

			 if ( response != 'nwf' ) {
			     // well-formed date
			     selected_input.parent().parent().addClass('table-success');
			     
			     setTimeout(
				 function () {
				     selected_input.parent().parent().removeClass('table-success');
				     selected_input.val(response);
				 },
				 1000
			     );
			 } else {
			     // non-well-formed date
			     selected_input.parent().parent().addClass('table-danger');
			     
			     setTimeout(
				 function () {
				     selected_input.parent().parent().removeClass('table-danger');
				     selected_input.val('');
				 },
				 1000
			     );
			 }
		     }

		 });
	     }

	 });

	 $('.date-restarted').on('input', function (e) {
	     selected_input = $(this);
	     trial_id = $(this).attr('trial_id');
	     newdate = $(this).val();

	     if (newdate == '') {
		 $.ajax ({
		     url: '<?php echo SITE_URL; ?>' + 'admin/comparator-clear-daterestarted.php',
		     type: 'post',
		     data: {
			 tid: trial_id
		     },
		     dataType: 'html'
		 }).done ( function (response) {

		     if ( response == '-') {

			 selected_input.parent().parent().addClass('table-success');

			 setTimeout(
			     function () {
				 selected_input.parent().parent().removeClass('table-success');
			     },
			     1000
			 );
		     }

		 });
		 
	     }

	     if (/^([0-9]{4,})-([0-9]{2,})-([0-9]{2,})$/.test(newdate)) {
		 
		 $.ajax ({
		     url: '<?php echo SITE_URL; ?>' + 'admin/comparator-daterestarted.php',
		     type: 'post',
		     data: {
			 tid: trial_id,
			 nd: newdate
		     },
		     dataType: 'html'
		 }).done ( function (response) {

		     if ( response != 'MySQL Error') {

			 if ( response != 'nwf' ) {
			     // well-formed date
			     selected_input.parent().parent().addClass('table-success');
			     
			     setTimeout(
				 function () {
				     selected_input.parent().parent().removeClass('table-success');
				     selected_input.val(response);
				 },
				 1000
			     );
			 } else {
			     // non-well-formed date
			     selected_input.parent().parent().addClass('table-danger');
			     
			     setTimeout(
				 function () {
				     selected_input.parent().parent().removeClass('table-danger');
				     selected_input.val('');
				 },
				 1000
			     );
			 }
		     }

		 });
	     }

	 });

	 $(document).ready(function () {

	     //$('.autocheck-include').each(function () {

		 //$(this).click();
		 
	     //});
	     
	 });
	 
	</script>
    </body>
</html>


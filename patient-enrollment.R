

## Patient enrollment

```{r patient-enrollment, echo=FALSE, message=FALSE, warning=FALSE}
library(readr)

# Trials in the Covid arm of the project
covid_trials <- read_delim(
  paste0("data/covid-arm-", Sys.Date() , ".tsv"),
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

# Trials in the Comparator arm
comparator_trials <- read_delim(
  paste0("data/comparator-arm-", Sys.Date() , ".tsv"),
  "\t",
  escape_double = FALSE,
  col_types = cols(
    date_stopped = col_date(format = "%Y-%m-%d"),
    restartdate = col_date(format = "%Y-%m-%d")
  ),
  na = "NULL",
  trim_ws = TRUE
)

# Trim Comparator arm to match length of Covid arm
comparator_trials <- subset(comparator_trials, date_stopped < max(covid_trials$date_stopped, na.rm=TRUE) - 365*2-1)

carm_end <- max(covid_trials$date_stopped, na.rm=TRUE)
comp_end <- carm_end - 365*2-1

c19e <- subset(covid_trials, covid19_explicit == 1)

# cumsum plot

carm_actual_enrol <- subset(covid_trials, enrollment_type == "Actual" & study_type == "Interventional")

carm_actual_enrol <- carm_actual_enrol[order(carm_actual_enrol$date_stopped),]

get_c19_enrol <- function ( c19, enrollment ) {

  if ( c19 == 1 ) {
    return (enrollment)
  } else {
    return (0)
  }

}

carm_actual_enrol$c19_actual_enrol <- mapply(
  function(x, y) get_c19_enrol (x, y),
  carm_actual_enrol$covid19_explicit,
  carm_actual_enrol$enrollment
)

carm_actual_enrol$anyreason_enrollment_cumsum <- cumsum(carm_actual_enrol$enrollment)
carm_actual_enrol$c19_enrollment_cumsum <- cumsum(carm_actual_enrol$c19_actual_enrol)

library(ggplot2)

ggplot(
  aes(
    x = date_stopped,
    y = anyreason_enrollment_cumsum
  ),
  data = carm_actual_enrol
) + geom_line() +
  labs(
    x = "Date of termination, suspension or withdrawal",
    y = "Actual enrollment",
    title = "Cumulative actual patient enrollment in interventional clinical trials that were stopped"
  ) + geom_line(
    aes(
      x = date_stopped,
      y = c19_enrollment_cumsum
    ),
    data = carm_actual_enrol
  ) + scale_y_continuous(
  )

```

The jump at 2020-04-23 is a result of a single clinical trial NCT04269824, which had an actual enrollment of 22763 patients.
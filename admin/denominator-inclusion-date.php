<?php

// This is not supposed to be called from the browser
// This script is just a support script that's supposed
// to be called by AJAX from denominator-inclusion.php

include_once ("../config.php");

$nct_id = $_POST['nctid'];
$version_index = $_POST['versionindex'];

$context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));

$ctg_index_url = "https://clinicaltrials.gov/ct2/history/" . $nct_id . "?V_" . $version_index . "=View";

$remote_index_file = file_get_contents($ctg_index_url, FALSE, $context);

$overall_status = NULL;

if ( $remote_index_file ) {

    // echo $ctg_index_url . "\n\n";

    $regex = '/Overall Status:<\/td>\n[ ]+<td>\n([A-Za-z, ]+)<\//';
    
    preg_match_all (
        $regex,
        $remote_index_file,
        $status,
        PREG_UNMATCHED_AS_NULL
    );

    foreach ($status[1] as $stat) {
	
	// echo "Overall status: " . trim($stat) . "\n";

	$overall_status = trim($stat);
    }

    if (
	$overall_status == "Not yet recruiting" |
	$overall_status == "Recruiting" |
	$overall_status == "Enrolling by invitation" |
	$overall_status == "Active, not recruiting"
    ) {
	echo "1";
    } else {
	echo "0";
    }

} else {
    echo "error";
}

?>

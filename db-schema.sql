-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: mysql.bgcarlisle.com
-- Generation Time: Apr 05, 2020 at 09:53 AM
-- Server version: 5.7.28-log
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `covid19trials`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `countries_comparator`
--

CREATE TABLE `countries_comparator` (
  `id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indications`
--

CREATE TABLE `indications` (
  `id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `indication` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indications_comparator`
--

CREATE TABLE `indications_comparator` (
  `id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `indication` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `interventions`
--

CREATE TABLE `interventions` (
  `id` int(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `intervention_type` varchar(500) NOT NULL,
  `intervention_name` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `other_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `interventions_comparator`
--

CREATE TABLE `interventions_comparator` (
  `id` int(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `intervention_type` varchar(500) NOT NULL,
  `intervention_name` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `other_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `lead_or_collaborator` varchar(50) NOT NULL,
  `agency` varchar(50) NOT NULL,
  `agency_class` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sponsors_comparator`
--

CREATE TABLE `sponsors_comparator` (
  `id` int(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `lead_or_collaborator` varchar(50) NOT NULL,
  `agency` varchar(50) NOT NULL,
  `agency_class` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trials`
--

CREATE TABLE `trials` (
  `id` int(11) NOT NULL,
  `when_indexed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nct_id` varchar(11) NOT NULL,
  `overall_status` varchar(50) NOT NULL,
  `why_stopped` varchar(500) DEFAULT NULL,
  `primary_completion_date` date DEFAULT NULL,
  `primary_completion_date_type` varchar(50) DEFAULT NULL,
  `last_update_submitted` date DEFAULT NULL,
  `brief_title` varchar(500) NOT NULL,
  `official_title` varchar(1000) NOT NULL,
  `brief_summary` text,
  `detailed_description` text,
  `enrollment_type` varchar(50) DEFAULT NULL,
  `enrollment` int(11) DEFAULT NULL,
  `phase` varchar(50) DEFAULT NULL,
  `study_type` varchar(50) DEFAULT NULL,
  `allocation` varchar(50) DEFAULT NULL,
  `intervention_model` varchar(50) DEFAULT NULL,
  `primary_purpose` varchar(50) DEFAULT NULL,
  `masking` varchar(50) DEFAULT NULL,
  `include` int(11) DEFAULT NULL,
  `include_updated` timestamp NULL DEFAULT NULL,
  `covid19_explicit` int(11) DEFAULT NULL,
  `date_stopped` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trials_comparator`
--

CREATE TABLE `trials_comparator` (
  `id` int(11) NOT NULL,
  `nct_id` varchar(11) NOT NULL,
  `overall_status` varchar(50) NOT NULL,
  `why_stopped` varchar(500) DEFAULT NULL,
  `primary_completion_date` date DEFAULT NULL,
  `primary_completion_date_type` varchar(50) DEFAULT NULL,
  `last_update_submitted` date DEFAULT NULL,
  `brief_title` varchar(500) NOT NULL,
  `official_title` varchar(1000) NOT NULL,
  `brief_summary` text,
  `detailed_description` text,
  `enrollment_type` varchar(50) DEFAULT NULL,
  `enrollment` int(11) DEFAULT NULL,
  `phase` varchar(50) DEFAULT NULL,
  `study_type` varchar(50) DEFAULT NULL,
  `allocation` varchar(50) DEFAULT NULL,
  `intervention_model` varchar(50) DEFAULT NULL,
  `primary_purpose` varchar(50) DEFAULT NULL,
  `masking` varchar(50) DEFAULT NULL,
  `include` int(11) DEFAULT NULL,
  `include_updated` timestamp NULL DEFAULT NULL,
  `date_stopped` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_comparator`
--
ALTER TABLE `countries_comparator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indications`
--
ALTER TABLE `indications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indications_comparator`
--
ALTER TABLE `indications_comparator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interventions`
--
ALTER TABLE `interventions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interventions_comparator`
--
ALTER TABLE `interventions_comparator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors_comparator`
--
ALTER TABLE `sponsors_comparator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trials`
--
ALTER TABLE `trials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nct_id` (`nct_id`);

--
-- Indexes for table `trials_comparator`
--
ALTER TABLE `trials_comparator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nct_id` (`nct_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries_comparator`
--
ALTER TABLE `countries_comparator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indications`
--
ALTER TABLE `indications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indications_comparator`
--
ALTER TABLE `indications_comparator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interventions`
--
ALTER TABLE `interventions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interventions_comparator`
--
ALTER TABLE `interventions_comparator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponsors_comparator`
--
ALTER TABLE `sponsors_comparator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trials`
--
ALTER TABLE `trials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trials_comparator`
--
ALTER TABLE `trials_comparator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

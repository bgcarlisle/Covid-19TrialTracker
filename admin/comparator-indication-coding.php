<?php

// To monitor while it's going:
// SELECT * FROM `trials_comparator` WHERE `include_2017` = 1 AND `cancer_indication` IS NOT NULL
// Denominator:
// SELECT COUNT(*) FROM `trials_comparator` WHERE `include_2017` = 1

include_once ("../config.php");

$trials = get_included_comparator_trials_with_indications_not_specified (5);

foreach ( $trials as $trial ) {

    echo $trial['nct_id'] . "<br>\n";

    $oncology = NULL;
    $neuro = NULL;
    $cv = NULL;
    $pain = NULL;

    if ( nct_in_indication ($trial['nct_id'], "oncology") ) {
	$oncology = TRUE;
	echo "Cancer ";
    } else {
	$oncology = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "neurologic") ) {
	$neuro = TRUE;
	echo "Neuro ";
    } else {
	$neuro = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "cardiovascular") ) {
	$cv = TRUE;
	echo "CV ";
    } else {
	$cv = FALSE;
    }

    if ( nct_in_indication ($trial['nct_id'], "pain") ) {
	$pain = TRUE;
	echo "Pain ";
    } else {
	$pain = FALSE;
    }

    echo "<br>\n";
    
    if ( ! is_null ($oncology) & ! is_null ($neuro) & ! is_null ($cv) & ! is_null ($pain) ) {
	if (update_comparator_indications ($trial['id'], $oncology, $neuro, $cv, $pain)) {
	    echo "Updated database<br><br>\n\n";
	} else {
	    echo "Database error<br><br>\n";
	}
    }
    
}

if ( count ($trials) > 0 ) {
?><script>
   setTimeout(function () {
       location.reload();
   }, 0); // Set this to a number of thousandths of a second to wait between refreshes
</script><?php
	 }

	 ?>

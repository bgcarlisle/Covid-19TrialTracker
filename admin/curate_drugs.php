<?php

include_once ("../config.php");

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Clinical trials stopped by Covid-19</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- Stopped Covid-19 trials CSS -->
	<link href="<?php echo SITE_URL; ?>covid-19.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>Admin: Curate drug list</h1>

		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <th scope="col">Type</th>
			    <th scope="col">Name</th>
			    <th scope="col">Description</th>
			    <th scope="col">Other name</th>
			    <th scope="col">Suggestion</th>
			    <th scope="col" style="min-width: 200px; text-align: right;">Curated name</th>
			</thead>
			<tbody>
			    <?php

			    $rows = get_drug_curation_table ();

			    foreach ( $rows as $row ) {

			    ?><tr id="row-<?php echo $row['id']; ?>">
				<th scope="row"><a href="https://clinicaltrials.gov/ct2/show/<?php echo $row['nct_id']; ?>" target="_blank"><?php echo $row['nct_id']; ?></a></th>
				<td onclick="$('#curateddrug-<?php echo $row['id']; ?>').val('<?php echo $row['int_name']; ?>');$('#row-<?php echo $row['id']; ?>').removeClass('table-success');"><?php echo $row['int_name']; ?></td>
				<td><?php echo $row['description']; ?></td>
				<td onclick="$('#curateddrug-<?php echo $row['id']; ?>').val('<?php echo $row['other_name']; ?>');$('#row-<?php echo $row['id']; ?>').removeClass('table-success');"><?php echo $row['other_name']; ?></td>
				<td onclick="$('#curateddrug-<?php echo $row['id']; ?>').val('<?php echo $row['suggestion']; ?>');$('#row-<?php echo $row['id']; ?>').removeClass('table-success');"><?php echo $row['suggestion']; ?></td>
				<td>
				    <div class="input-group">
					<input type="text" id="curateddrug-<?php echo $row['id']; ?>" class="form-control curateddrug" placeholder="-" aria-label="Curated drug name" value="<?php echo $row['curated']; ?>">
					<div class="input-group-append">
					    <button class="btn btn-primary" type="button" onclick="click_save(<?php echo $row['id']; ?>);">Save</button>
					</div>
				</div>
			    </td>
			    </tr><?php
				 
				 }

				 ?>
			</tbody>
		    </table>

		    <a href="<?php echo SITE_URL; ?>admin/curate_drugs.php" class="btn btn-primary btn-block" style="margin-bottom: 40px;">More</a>
		</div>
	    </div>
	</div>
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- Stopped Covid-19 trials JS -->
	<script>
	 
	 function click_save (row_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>' + 'admin/save_druglist_curation.php',
		 type: 'post',
		 data: {
		     rid: row_id,
		     newvalue: $('#curateddrug-' + row_id).val()
		 },
		 dataType: 'html'
	     }).done ( function (response) {

		 if (response == "Saved") {

		     $('#row-' + row_id).addClass('table-success');

		 } else {

		     $('#row-' + row_id).addClass('table-warning');

		     setTimeout(function () {
			 $('#row-' + row_id).removeClass('table-warning');
		     },5000);
		 }
		 
	     });
	     
	 }

	 $('.curateddrug').on('input', function (e) {

	     selected_input = $(this);

	     selected_input.parent().parent().parent().removeClass('table-success');
	     
	 });

	</script>
    </body>
</html>

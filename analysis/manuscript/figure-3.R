library(tidyverse)
library(ggplot2)

## Read the covid arm TSV
c19trials <- read_delim(
  "../data/covid-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Read the comparator arm TSV
comptrials <- read_delim(
  "../data/comparator-arm-2021-01-25.tsv",
  "\t",
  escape_double = FALSE,
  trim_ws = TRUE,
  na = "NULL"
)

## Only look at interventional trials
c19trials <- c19trials %>%
    filter(study_type == "Interventional")

comptrials <- comptrials %>%
    filter(study_type == "Interventional")

## Remove data points from outside 1-year sample
c19trials <- c19trials %>%
    filter(date_stopped < as.Date("2020-12-01"))

## Sort by date
c19trials <- c19trials %>%
    arrange(date_stopped)

comptrials <- comptrials %>%
    arrange(date_stopped)

## Only consider trials with an actual enrolment
## greater than zero
c19trials <- c19trials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

comptrials <- comptrials %>%
    filter(
        enrollment_type == "Actual",
        enrollment > 0
    )

## Make a subset that includes only trials stopped for reasons
## that explicitly cite Covid-19
c19e <- c19trials %>%
    filter(covid19_explicit == 1)

## Put them all together
boxplotc19 <- c19trials %>%
    select(enrollment, covid19_explicit) %>%
    rename (arm = covid19_explicit)

boxplotcomp <- comptrials %>%
    select(enrollment)

boxplotcomp$arm <- 2

boxplot <- bind_rows(
    boxplotc19,
    boxplotcomp
)

ggplot(
    aes(
        group = arm,
        y = enrollment
    ),
    data = boxplot
) +
    geom_boxplot()
